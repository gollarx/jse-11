package ru.t1.shipilov.tm.model;

import java.util.UUID;

public final class Project {

    private final String id = UUID.randomUUID().toString();;

    private String name;

    private String description;

    public Project() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
