package ru.t1.shipilov.tm.api.service;

import ru.t1.shipilov.tm.api.repository.IProjectRepository;
import ru.t1.shipilov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
